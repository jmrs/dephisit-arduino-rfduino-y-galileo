﻿# **DEPHISIT - [Arduino, RFduino y Galileo]** #

Los proyectos y librerías Arduino presentes en este repositorio se corresponden con los desarrollos realizados sobre las plataformas Arduino, RFduino e Intel Galileo para el proyecto DEPHISIT.


# Descripción de los proyectos y librerías desarrollados #

A continuación, se comentan los distintos proyectos y librerías Arduino desarrolladas, así como las dependencias/requisitos con otras librerías Arduino externas:

- Librería BycicleDetector. Define e implementa el funcionamiento de los diversos dispositivos DEPHISIT de tipo BycicleDetector. En la actualidad tan sólo se ha definido e implementado un dispositivo de este tipo basado en dos [sensores de inclinación](http://es.rs-online.com/web/p/interruptores-de-inclinacion-sin-mercurio/3615087/) y un [interruptor magnético de tipo "Reed"](http://es.rs-online.com/web/p/interruptores-de-laminas/3622546/) (BicycleDetectorBasedOn2TiltAndReed).

- Librería TrafficLightDetector. Define e implementa el funcionamiento de los diversos dispositivos DEPHISIT de tipo TrafficLightDetector. En la actualidad tan sólo se ha definido e implementado un dispositivo de este tipo basado en tres [sensores de fotoeléctrico de tipo LDR](http://es.rs-online.com/web/p/resistores-dependientes-de-luz-ldr/4558036/) (TrafficLightDetectorBasedOn3LDR).

- Librería WeatherDetector. Define e implementa el funcionamiento de los diversos dispositivos DEPHISIT de tipo WeatherDetector. En la actualidad tan sólo se ha definido e implementado un dispositivo de este tipo basado en una [placa Grove con sensor de lluvia](http://www.seeedstudio.com/depot/Grove-Water-Sensor-p-748.html) y otro [placa Grove con sensor de temperatura/humedad](http://www.seeedstudio.com/depot/Grove-TemperatureHumidity-Sensor-Pro-p-838.html) (WeatherDetectorBasedOnGRainAndGHTPro) y depende de la librería externa [Grove_Temperature_And_Humidity_Sensor](https://github.com/Seeed-Studio/Grove_Temperature_And_Humidity_Sensor).

- Librería DisplayActuator. Define e implementa el funcionamiento de los diversos dispositivos DEPHISIT de tipo DisplayActuator. En la actualidad tan sólo se ha definido e implementado un dispositivo de este tipo basado en una [placa Grove con pantalla de tipo LCD](http://www.seeedstudio.com/depot/Grove-LCD-RGB-Backlight-p-1643.html) (DisplayActuatorBasedOnGroveLCD) y depende de la librería externa [Grove - LCD RGB Backlight](https://github.com/Seeed-Studio/Grove_LCD_RGB_Backlight).

- Librería LightActuator. Define e implementa el funcionamiento de los diversos dispositivos DEPHISIT de tipo LightActuator. En la actualidad tan sólo se ha definido e implementado un dispositivo de este tipo basado en una [placa Grove con emisor de luz de tipo LED](http://www.seeedstudio.com/depot/Grove-Red-LED-p-1142.html) (LightActuatorBasedOnGroveLED).

- Librería SeatSensor. Define e implementa el funcionamiento de los diversos dispositivos DEPHISIT de tipo SeatSensor. En la actualidad tan sólo se ha definido e implementado un dispositivo de este tipo basado en un [interruptor magnético de tipo "Reed"](http://es.rs-online.com/web/p/interruptores-de-laminas/3622546/) y un [sensor de presión de tipo FSR](http://www.exp-tech.de/square-force-sensitive-resistor-fsr-interlink-406) (SeatSensorBasedOnReedAndFSR).

- Librería SoundActuator. Define e implementa el funcionamiento de los diversos dispositivos DEPHISIT de tipo SoundActuator. En la actualidad tan sólo se ha definido e implementado un dispositivo de este tipo basado en una [placa Grove con emisor de sonidos de tipo "Buzzer"](http://www.seeedstudio.com/depot/Grove-Buzzer-p-768.html) (SoundActuatorBasedOnGroveBuzzer).

- Proyecto BycicleDetectorBeacon. Baliza para bicicletas, implementada para la plataforma RFduino. Hace uso de la librería BycicleDetector comentada más arriba.

- Proyecto StopSignBeacon. Baliza para señales de STOP, implementada para la plataforma RFduino.

- Proyecto TrafficLightDetectorBeacon. Baliza para semáforos, implementada para la plataforma RFduino. Hace uso de la librería TrafficLightDetector comentada más arriba.

- Proyecto SeatSensorBLE. Sensor BLE para el asiento (dispositivo DEPHISIT de tipo "SeatSensor"), implementado para la plataforma RFduino. Hace uso de la librería SeatSensor comentada más arriba.

- Proyecto SeatSensorWithNotificationsBLE. Sensor BLE con notificaciones para el asiento (dispositivo DEPHISIT de tipo "SeatSensorWithNotifications"), implementado para la plataforma RFduino. Hace uso de la librería SeatSensor comentada más arriba y depende de la librería externa de [RFduino](https://github.com/RFduino/RFduino).

- Proyecto SimulatedTemperatureSensorBLE. Sensor BLE simulado de temperatura (dispositivo DEPHISIT de tipo "TemperatureSensor"), implementado para la plataforma RFduino.

- Proyecto WeatherDetectorBeacon. Baliza para estado (condiciones atmosféricas) de la carretera, implementada para la plataforma RFduino. Hace uso de la librería WeatherDetector comentada más arriba.

- Proyecto NRF_Display_Light_Sound_Actuators_IntelGalileo. Implementación del actuador visual (dispositivo de tipo "DisplayActuator"), luminoso (dispositivo de tipo "LightActuator") y sonoro (dispositivo de tipo "SoundActuator") de la plataforma DEPHISIT, implementados para la plataforma Intel Galileo y con comunicación a través del chip RF24. Depende de la librería externa [Arduino driver for nRF24L01 2.4GHz Wireless Transceiver](https://github.com/maniacbug/RF24), y de las librerías criptográficas [AES](https://github.com/spaniakos/AES) y [MD5](https://github.com/spaniakos/ArduinoMD5).

También se han incluido los proyectos inicialmente desarrollados para los prototipos hardware relativos a las señales de tráfico. Cabe destacar que el formato de las notificaciones/advertencias emitidas por estos proyectos Arduino no son compatibles con la versión 0.1 del servicio DEPHISIT desarrollado en Android.

- Proyecto TrafficLightDetectorBeacon_Antiguo. Baliza para semáforos, implementada para su uso por los primeros prototipos hardware realizados en base a la plataforma RFduino. Hace uso de la librería TrafficLightDetector comentada más arriba.

- Proyecto StopSignBeacon_Antiguo. Baliza para señales de STOP, implementada para su uso por los primeros prototipos hardware realizados en base a la plataforma RFduino.

*NOTA: Estos son los proyectos (código) Arduino que tenían cargados aquellos prototipos hardware mostrados en la reunión de Abril de 2015. Por lo tanto, para que esos prototipos hardware vuelvan a funcionar correctamente con la versión 0.1 del servicio DEPHISIT, debe ser reprogramados/recargados con la nueva versión de los correspondientes proyectos Arduino.*
